﻿# การติดตั้ง Module
- Unzip repository
- เข้าเมนู Modules

![enter image description here](https://worldwallet.net/img_bitbucket/presta/Module.png)

- กดปุ่ม Upload a module

![enter image description here](https://worldwallet.net/img_bitbucket/presta/UploadModule.png)

- เลือกไฟล์ moneyspacepayment.zip , moneyspacepaymentqrcodepromptpay.zip moneyspacepaymentinstallment.zip ที่ท่านแตกไฟล์ repository

![enter image description here](https://worldwallet.net/img_bitbucket/presta/select.png)

- กรณีติดตั้งไม่ได้กดอัพโหลดติดตั้งใหม่

****

# Changelog
- 2023-05-28 : fixed bug
- 2022-04-05 : Updated module
- 2022-01-01 : Updated readme and module
- 2021-06-08 : Updated readme
- 2021-06-08 : Updated idpay
- 2020-10-23 : Updated the order status system.
- 2020-02-03 : Added installment payments and update QR Code Promptpay (qrnone)
- 2019-12-20 : Fixed bugs and update redirect system
- 2019-12-14 : Update order id in QR Code Promptpay and fixed bugs
- 2019-11-25 : Update QR Code Promptpay
- 2019-02-11 : Added